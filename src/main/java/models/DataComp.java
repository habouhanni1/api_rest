package models;

import java.util.HashMap;
import java.util.Map;

public class DataComp {

    public static Map<Integer,Intervalle> fibres = new HashMap<>();
    public static Map<Integer,Intervalle> proteines = new HashMap<>();;

    public static Map<Integer,Intervalle> densite = new HashMap<>();;
    public static Map<Integer,Intervalle> graisses = new HashMap<>();;
    public static Map<Integer,Intervalle> sucres = new HashMap<>();;
    public static Map<Integer,Intervalle> sodium = new HashMap<>();;

    public DataComp(){
        fibres.put(0,new Intervalle(0 ,  0.9));
        fibres.put(1,new Intervalle(0.9 ,  1.9));
        fibres.put(2,new Intervalle(1.9 ,  2.8));
        fibres.put(3,new Intervalle(2.8 ,  3.7));
        fibres.put(4,new Intervalle(3.7 ,  4.7));
        fibres.put(5,new Intervalle(4.7 ,  100));

        proteines.put(0,new Intervalle(0 ,  1.6));
        proteines.put(1,new Intervalle(1.6 ,  3.2));
        proteines.put(2,new Intervalle(3.2 ,  4.8));
        proteines.put(3,new Intervalle(4.8 ,  6.4));
        proteines.put(4,new Intervalle(6.4 ,  8.0));
        proteines.put(5,new Intervalle(8.0 ,  100));

        densite.put(0,new Intervalle(0 ,  335));
        densite.put(1,new Intervalle(335,  670));
        densite.put(2,new Intervalle(670 ,  1005));
        densite.put(3,new Intervalle(1005 ,  1340));
        densite.put(8,new Intervalle(1340 ,  1675));
        densite.put(4,new Intervalle(1675 ,  2010));
        densite.put(5,new Intervalle(2010 ,  2345));
        densite.put(6,new Intervalle(2345 ,  2680));
        densite.put(7,new Intervalle(2680 ,  3015));
        densite.put(9,new Intervalle(3015 ,  3350));
        densite.put(10,new Intervalle(3350 ,  10000));

        graisses.put(0,new Intervalle(0 ,  1));
        graisses.put(1,new Intervalle(1,  2));
        graisses.put(2,new Intervalle(2 ,  3));
        graisses.put(3,new Intervalle(3 ,  4));
        graisses.put(8,new Intervalle(4 ,  5));
        graisses.put(4,new Intervalle(5 ,  6));
        graisses.put(5,new Intervalle(6 ,  7));
        graisses.put(6,new Intervalle(7 ,  8));
        graisses.put(7,new Intervalle(8 ,  9));
        graisses.put(9,new Intervalle(9 ,  10));
        graisses.put(10,new Intervalle(10 ,  11));


        sucres.put(0,new Intervalle(0 ,  4.5));
        sucres.put(1,new Intervalle(4.5,  9));
        sucres.put(2,new Intervalle(9,  13.5));
        sucres.put(3,new Intervalle(13.5 ,  18));
        sucres.put(8,new Intervalle(18 ,  22.5));
        sucres.put(4,new Intervalle(22.5 ,  27));
        sucres.put(5,new Intervalle(27 ,  31));
        sucres.put(6,new Intervalle(31,  36));
        sucres.put(7,new Intervalle(36 ,  40));
        sucres.put(9,new Intervalle(40 ,  45));
        sucres.put(10,new Intervalle(45 ,  100));

        sucres.put(0,new Intervalle(0 ,  90));
        sucres.put(1,new Intervalle(90,  9));
        sucres.put(2,new Intervalle(180,  13.5));
        sucres.put(3,new Intervalle(270 ,  18));
        sucres.put(8,new Intervalle(360 ,  22.5));
        sucres.put(4,new Intervalle(450 ,  27));
        sucres.put(5,new Intervalle(27 ,  31));
        sucres.put(6,new Intervalle(31,  36));
        sucres.put(7,new Intervalle(36 ,  40));
        sucres.put(9,new Intervalle(40 ,  45));
        sucres.put(10,new Intervalle(45 ,  100));
https://gitlab.isima.fr/javapro/caillou/tree/master
    }



}
