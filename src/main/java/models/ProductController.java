package models;

import com.mashape.unirest.http.exceptions.UnirestException;
import models.Product;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class ProductController {
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/product")
    public Product product(@RequestParam(value="name", defaultValue="World") String name) throws UnirestException {
        Product p= new Product();
        return p.getProductByQR("3029330003533");
    }
}
