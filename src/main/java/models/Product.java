package models;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;

public class Product {

    private  String barCode;
    private  String content;
    private  int energy;
    private  int saturated_fat;
    private  double sugars;
    private  int salt_100g;
    private  double proteins;
    private  double fiber;
    private  double score;


    public Product(String barCode, String content) {
        this.barCode = barCode;
        this.content = content;
    }

    public Product(String codebar,int energy, int saturated_fat, double sugars, int salt_100g, double proteins, double fiber) {
        this.energy = energy;
        this.saturated_fat = saturated_fat;
        this.sugars = sugars;
        this.salt_100g = salt_100g;
        this.proteins = proteins;
        this.fiber = fiber;
        this.barCode=codebar;
    }

    public Product() {
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public Product getProduct(String barCode) throws UnirestException {
      /*  JSONObject jsonResponse
                = Unirest.get("https://fr.openfoodfacts.org/api/v0/produit/3029330003533.json")
                .header("accept", "application/json").queryString("apiKey", "123")
                .asJson()
                .getBody()
                .getObject();


        JSONObject nutJson = jsonResponse.getJSONObject("product").getJSONObject("nutriments");
        Product p=new Product(nutJson.getInt("energy_100g"), nutJson.getInt("saturated-fat_100g"),
                nutJson.getDouble("sugars_100g"), nutJson.getInt("salt_100g"),
                nutJson.getInt("fiber_100g"), nutJson.getDouble("proteins_100g"));
        return p;*/
      return null;
    }

    public Product getProductByQR(String barCode) throws UnirestException {

        JSONObject jsonResponse
                = Unirest.get("https://fr.openfoodfacts.org/api/v0/produit/"+barCode+".json")
                .header("accept", "application/json").queryString("apiKey", "123")
                .asJson()
                .getBody()
                .getObject();


        JSONObject nutJson = jsonResponse.getJSONObject("product").getJSONObject("nutriments");
        Product p=new Product(barCode,nutJson.getInt("energy_100g"), nutJson.getInt("saturated-fat_100g"),
                nutJson.getDouble("sugars_100g"), nutJson.getInt("salt_100g"),
                nutJson.getInt("fiber_100g"), nutJson.getDouble("proteins_100g"));
        return p;
    }
}
