package models;

public class Intervalle {
    private double bi;
    private double bs;


    public Intervalle(double bi , double bs) {
        this.bi = bi;
        this.bs = bs;
    }

    public boolean in(int valeur){
        return (valeur < bs && bi < valeur );
    }
}
